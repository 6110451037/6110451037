package Cinema;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import java.io.*;
import java.util.ArrayList;

public class Register {
    public int visa=0;
    @FXML
    Button toLogin,submitRegister;
    @FXML
    ImageView alert1,alert2,alert3,alert4,alert5;
    @FXML
    Label used;
    @FXML
    TextField regisFirstName,regisLastName,regisEmail,regisUsername,regisPassword;
    ArrayList<Account> accounts = new ArrayList<>();

    @FXML
    public void submitRegisterAction (ActionEvent event) throws IOException {
        File file = new File("user.txt");
        FileWriter fileWriter = new FileWriter(file,true);
        BufferedWriter writer = new BufferedWriter(fileWriter);
        if((regisFirstName.getText().equals(""))||(regisLastName.getText().equals(""))||(regisEmail.getText().equals(""))||(regisUsername.getText().equals(""))||(regisPassword.getText().equals(""))){
            used.setVisible(false);
            if(regisFirstName.getText().equals("")){
                alert1.setVisible(true);
            }else{
                alert1.setVisible(false);
            }
            if(regisLastName.getText().equals("")){
                alert2.setVisible(true);
            }else{
                alert2.setVisible(false);
            }
            if(regisEmail.getText().equals("")){
                alert3.setVisible(true);
            }else{
                alert3.setVisible(false);
            }
            if(regisUsername.getText().equals("")){
                alert4.setVisible(true);
            }else{
                alert4.setVisible(false);
            }
            if(regisPassword.getText().equals("")){
                alert5.setVisible(true);
            }else{
                alert5.setVisible(false);
            }

        }else{
            alert1.setVisible(false);
            alert2.setVisible(false);
            alert3.setVisible(false);
            alert4.setVisible(false);
            alert5.setVisible(false);
            try {
                FileReader fileReader = new FileReader("user.txt");
                BufferedReader reader = new BufferedReader(fileReader);
                String line = "";
                while ((line = reader.readLine()) != null){
                    String[] d = line.split(",");
                    Account account = new Account(d[0],d[1],d[2],d[3],d[4]);
                    accounts.add(account);
                }
                reader.close();
            } catch (IOException e) {
                e.getMessage();
            }
            for(Account ac: accounts){
                if(ac.getUsername().equals(regisUsername.getText())) {
                    visa += 1;
                }
            }
            if (visa==0){
                writer.append(regisFirstName.getText()+","+regisLastName.getText()+","+regisEmail.getText()+","+regisUsername.getText()+","+regisPassword.getText());
                writer.newLine();
                writer.close();
                FXMLLoader scene = new FXMLLoader(getClass().getResource("login.fxml"));
                Stage layer = (Stage) submitRegister.getScene().getWindow();
                Scene page = new Scene(scene.load());
                layer.setScene(page);
            }else{
                used.setVisible(true);
            }
        }
    }

    public void toLoginAction(ActionEvent event) throws IOException {
        FXMLLoader scene = new FXMLLoader(getClass().getResource("login.fxml"));
        Stage layer = (Stage) toLogin.getScene().getWindow();
        Scene page = new Scene(scene.load());
        layer.setScene(page);
    }
}
