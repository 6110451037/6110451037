package Cinema;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.scene.control.Button;
import java.io.File;
import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import java.io.BufferedReader;
import java.io.FileReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;

public class Login implements Initializable{
    @FXML
    ImageView alert1,alert2;
    @FXML
    Button toRegister,toApp;
    @FXML
    TextField enterUsername;
    @FXML
    PasswordField enterPassword;
    @FXML
    Label incorrect;
    ArrayList<Account> accounts = new ArrayList<>();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        File fileUser = new File("user.txt");
        if(!fileUser.exists()){
            try {
                fileUser.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            FileReader fileReader = new FileReader(fileUser);
            BufferedReader reader = new BufferedReader(fileReader);
            String line = "";
            while ((line = reader.readLine()) != null){
                String[] d = line.split(",");
                Account account = new Account(d[0],d[1],d[2],d[3],d[4]);
                accounts.add(account);
            }
            reader.close();
        } catch (IOException e) {
            e.getMessage();
        }
    }

    @FXML
    private void toAppAction(ActionEvent event) throws IOException{
        for(Account ac: accounts){
            if(ac.getUsername().equals(enterUsername.getText()) && ac.getPassword().equals(enterPassword.getText())){
                incorrect.setText("");
                alert1.setVisible(false);
                alert2.setVisible(false);
                FXMLLoader scene = new FXMLLoader(getClass().getResource("selectMovie.fxml"));
                Stage layer = (Stage) toApp.getScene().getWindow();
                Scene page = new Scene(scene.load());
                layer.setScene(page);
            }else{
                if(enterUsername.getText().equals("")||enterPassword.getText().equals("")){
                    incorrect.setText("");
                    alert1.setVisible(false);
                    alert2.setVisible(false);
                    if(enterUsername.getText().equals("")){
                        alert1.setVisible(true);
                    }if(enterPassword.getText().equals("")){
                        alert2.setVisible(true);
                    }
                }else if(ac.getUsername().equals(enterUsername.getText())){
                    alert1.setVisible(false);
                    alert2.setVisible(false);
                    incorrect.setText("*invalid password*");
                    break;
                }else{
                    alert1.setVisible(false);
                    alert2.setVisible(false);
                    incorrect.setText("*unknow username*");
                }
            }
        }
    }
    public void toRegisterAction(javafx.event.ActionEvent actionEvent) throws IOException {
        FXMLLoader scene = new FXMLLoader(getClass().getResource("register.fxml"));
        Stage layer = (Stage) toRegister.getScene().getWindow();
        Scene page = new Scene(scene.load());
        layer.setScene(page);
    }

}
