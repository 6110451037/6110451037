package Cinema;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class JokerThree implements Initializable {
    @FXML
    CheckBox a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,
            b1,b2,b3,b4,b5,b6,b7,b8,b9,b10,
            aa1,aa2,aa3,aa4;

    @FXML
    Label output;

    @FXML
    Button back;

    ArrayList<String> selected = new ArrayList<>();
    ArrayList<CheckBox> NormalSeat = new ArrayList<>();
    ArrayList<CheckBox> VipSeat = new ArrayList<>();
    private int price = 0;
    private int count = 0;
    private String st = "";

    public void initialize(URL location, ResourceBundle resources) {
        NormalSeat.add(a1);
        NormalSeat.add(a2);
        NormalSeat.add(a3);
        NormalSeat.add(a4);
        NormalSeat.add(a5);
        NormalSeat.add(a6);
        NormalSeat.add(a7);
        NormalSeat.add(a8);
        NormalSeat.add(a9);
        NormalSeat.add(a10);
        NormalSeat.add(b1);
        NormalSeat.add(b2);
        NormalSeat.add(b3);
        NormalSeat.add(b4);
        NormalSeat.add(b5);
        NormalSeat.add(b6);
        NormalSeat.add(b7);
        NormalSeat.add(b8);
        NormalSeat.add(b9);
        NormalSeat.add(b10);
        VipSeat.add(aa1);
        VipSeat.add(aa2);
        VipSeat.add(aa3);
        VipSeat.add(aa4);
        ArrayList<String> seat = Read.readFile("joker","3");
        for(CheckBox c : NormalSeat){
            if(Read.booked(c.getId(),seat)){
                c.setIndeterminate(true);
                c.setDisable(true);
                c.setOpacity(1);
            }
        }
        for(CheckBox c : VipSeat){
            if(Read.booked(c.getId(),seat)){
                c.setIndeterminate(true);
                c.setDisable(true);
                c.setOpacity(1);
            }
        }
    }

    public void Select(ActionEvent event) {
        for (int i = 0; i < NormalSeat.size(); i++) {
            if (NormalSeat.get(i).equals(event.getSource())) {
                if (NormalSeat.get(i).isSelected() == true) {
                    price += 250;
                    count += 1;
                    if(count>1){
                        st = "seats";
                    }else{
                        st = "seat";
                    }
                    output.setText("You selected "+count+" "+st+" Price: "+price+" Baths");
                } else if (NormalSeat.get(i).isSelected() == false) {
                    price -= 250;
                    count -= 1;
                    if(count>1){
                        st = "seats";
                    }else{
                        st = "seat";
                    }
                    output.setText("You selected "+count+" "+st+" Price: "+price+" Baths");
                }
            }
            if(count==0){
                output.setText("");
            }
        }
        for (int i = 0; i < VipSeat.size(); i++) {
            if (VipSeat.get(i).equals(event.getSource())) {
                if (VipSeat.get(i).isSelected() == true) {
                    price += 500;
                    count += 1;
                    if(count>1){
                        st = "seats";
                    }else{
                        st = "seat";
                    }
                    output.setText("You selected "+count+" "+st+" Price: "+price+" Baths");
                } else if (VipSeat.get(i).isSelected() == false) {
                    price -= 500;
                    count -= 1;
                    if(count>1){
                        st = "seats";
                    }else{
                        st = "seat";
                    }
                    output.setText("You selected "+count+" "+st+" Price: "+price+" Baths");
                }
            }
            if(count==0){
                output.setText("");
            }
        }
        CheckBox b = (CheckBox) event.getSource();
        if(b.isSelected() == true)
            selected.add(b.getId());
        else
            selected.remove(b.getId());
    }

    @FXML
    public void backAction(javafx.event.ActionEvent actionEvent) throws IOException {
        FXMLLoader scene = new FXMLLoader(getClass().getResource("selectCinemaJoker.fxml"));
        Stage layer = (Stage) back.getScene().getWindow();
        Scene page = new Scene(scene.load());
        layer.setScene(page);
    }

    @FXML public void next(ActionEvent event) throws IOException{
        if(selected.size() == 0){
            Alert alert = new Alert(Alert.AlertType.INFORMATION,"Please select seat", ButtonType.OK,ButtonType.NO);
            alert.showAndWait();
            return;
        }
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION,"Confirm booking  " + price + " Baths", ButtonType.OK,ButtonType.NO);
        alert.showAndWait();
        if(alert.getResult() == ButtonType.OK){
            Write.writeFile("joker","3",selected);
        }
        FXMLLoader scene = new FXMLLoader(getClass().getResource("selectMovie.fxml"));
        Stage layer = (Stage) back.getScene().getWindow();
        Scene page = new Scene(scene.load());
        layer.setScene(page);
    }
}
