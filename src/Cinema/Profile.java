package Cinema;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.io.IOException;

public class Profile {
    @FXML
    Button toLobby;

    @FXML
    public void toLobbyAction(javafx.event.ActionEvent actionEvent) throws IOException {
        FXMLLoader scene = new FXMLLoader(getClass().getResource("selectMovie.fxml"));
        Stage layer = (Stage) toLobby.getScene().getWindow();
        Scene page = new Scene(scene.load());
        layer.setScene(page);
    }
}
