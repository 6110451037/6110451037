package Cinema;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Read {
    public static ArrayList<String> readFile(String movie,String theatre){
        ArrayList<String> seat = new ArrayList<>();
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader("BookingData.csv"));
            String line = null;
            while((line = bufferedReader.readLine()) != null){
                String[] data = line.split(",");
                if(data[0].equals(movie) && data[1].equals(theatre))
                    seat.add(data[2]);
            }
            bufferedReader.close();
        }catch (IOException e){
            e.getMessage();
        }
        return seat;
    }

    public static boolean booked(String s,ArrayList<String> seat){
        for(String st : seat){
            if(st.equals(s)){
                return true;
            }
        }
        return false;
    }
}
