package Cinema;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Write {
    public static void writeFile(String movie, String theatre, ArrayList<String> selected){
        BufferedWriter bufferedWriter;
        try {
            bufferedWriter = new BufferedWriter(new FileWriter("BookingData.csv",true));
            for(String s : selected) {
                bufferedWriter.write(movie + "," + theatre + "," +s);
                bufferedWriter.newLine();
            }
            bufferedWriter.flush();
            bufferedWriter.close();
        }catch (IOException e){
            e.getMessage();
        }
    }
}
