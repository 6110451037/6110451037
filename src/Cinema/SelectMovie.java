package Cinema;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import java.io.IOException;

public class SelectMovie {
    @FXML
    Button itbtn,jokerbtn,halloweenbtn,toLogin,toProfile;

    @FXML
    public void itbtnAction(javafx.event.ActionEvent actionEvent) throws IOException {
        FXMLLoader scene = new FXMLLoader(getClass().getResource("selectCinemaIt.fxml"));
        Stage layer = (Stage) itbtn.getScene().getWindow();
        Scene page = new Scene(scene.load());
        layer.setScene(page);
    }

    @FXML
    public void jokerbtnAction(javafx.event.ActionEvent actionEvent) throws IOException {
        FXMLLoader scene = new FXMLLoader(getClass().getResource("selectCinemaJoker.fxml"));
        Stage layer = (Stage) jokerbtn.getScene().getWindow();
        Scene page = new Scene(scene.load());
        layer.setScene(page);
    }

    @FXML
    public void halloweenbtnAction(javafx.event.ActionEvent actionEvent) throws IOException {
        FXMLLoader scene = new FXMLLoader(getClass().getResource("selectCinemaHalloween.fxml"));
        Stage layer = (Stage) halloweenbtn.getScene().getWindow();
        Scene page = new Scene(scene.load());
        layer.setScene(page);
    }

    @FXML
    public void toLoginAction(javafx.event.ActionEvent actionEvent) throws IOException {
        FXMLLoader scene = new FXMLLoader(getClass().getResource("login.fxml"));
        Stage layer = (Stage) toLogin.getScene().getWindow();
        Scene page = new Scene(scene.load());
        layer.setScene(page);
    }

    @FXML
    public void toProfileAction(javafx.event.ActionEvent actionEvent) throws IOException {
        FXMLLoader scene = new FXMLLoader(getClass().getResource("profile.fxml"));
        Stage layer = (Stage) toProfile.getScene().getWindow();
        Scene page = new Scene(scene.load());
        layer.setScene(page);
    }

}
