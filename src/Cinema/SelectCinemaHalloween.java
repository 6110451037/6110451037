package Cinema;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import java.io.IOException;

public class SelectCinemaHalloween {
    @FXML
    Button to1,to2,to3,to4,back;

    @FXML
    public void to1Action(javafx.event.ActionEvent actionEvent) throws IOException {
        FXMLLoader scene = new FXMLLoader(getClass().getResource("halloweenOne.fxml"));
        Stage layer = (Stage) to1.getScene().getWindow();
        Scene page = new Scene(scene.load());
        layer.setScene(page);
    }

    @FXML
    public void to2Action(javafx.event.ActionEvent actionEvent) throws IOException {
        FXMLLoader scene = new FXMLLoader(getClass().getResource("halloweenTwo.fxml"));
        Stage layer = (Stage) to2.getScene().getWindow();
        Scene page = new Scene(scene.load());
        layer.setScene(page);
    }

    @FXML
    public void to3Action(javafx.event.ActionEvent actionEvent) throws IOException {
        FXMLLoader scene = new FXMLLoader(getClass().getResource("halloweenThree.fxml"));
        Stage layer = (Stage) to3.getScene().getWindow();
        Scene page = new Scene(scene.load());
        layer.setScene(page);
    }

    @FXML
    public void to4Action(javafx.event.ActionEvent actionEvent) throws IOException {
        FXMLLoader scene = new FXMLLoader(getClass().getResource("halloweenFour.fxml"));
        Stage layer = (Stage) to4.getScene().getWindow();
        Scene page = new Scene(scene.load());
        layer.setScene(page);
    }

    @FXML
    public void backAction(javafx.event.ActionEvent actionEvent) throws IOException {
        FXMLLoader scene = new FXMLLoader(getClass().getResource("selectMovie.fxml"));
        Stage layer = (Stage) back.getScene().getWindow();
        Scene page = new Scene(scene.load());
        layer.setScene(page);
    }
}
